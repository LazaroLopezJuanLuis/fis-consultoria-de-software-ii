# **Consultoría de software II: Reporte** :office: :computer:

## **Nearsoft**
* **Logotipo**  
![Nearsoft](imagenes/Nearsoft.svg)  
* **Sitio web**  
[Nearsoft • Grow your Software Development Team](https://nearsoft.com)  
* **Ubicación**  
    - Hermosillo  
      [Blvd. Antonio Quiroga 21 Col. El Llano Hermosillo, Sonora, México 83210](https://goo.gl/maps/VuZtGMiwbV9sfyCn7)  
    - Chihuahua  
      [Calle 2da 2016 Col. Centro Chihuahua, Chihuahua, México 31000](https://goo.gl/maps/3i7CndVE458F5KrcA)  
    - San Luis Potosi  
      [Sierra Ventana 268 Lomas 3a Sección San Luis Potosí, S.L.P., México 78216](https://goo.gl/maps/ZbRy3r6P69n4wcmi8)  
    - Merida  
      [Calle 29 477 Col. Gonzalo Guerrero Merida, Yucatan, México 97119](https://goo.gl/maps/ZbRy3r6P69n4wcmi8)  
    - Cuidad de México (Cerrado Permanentemente según Google Maps)  
      [San Luis Potosí 196, 1er Piso Col. Roma Norte, Del. Cuauhtémoc Mexico City, Mexico 06700](https://goo.gl/maps/XC64an8G2ZgjCyGy5)  
* **Acerca de**  
Nearsoft tiene que ver con el desarrollo de software.  
Nuestros equipos se autogestionan y entregan a tiempo.  
Tratamos nuestro trabajo como un oficio y aprendemos unos de otros.  
Constantemente subimos el listón.  
* **Servicios**  
    - Servicios de diseño de sistemas de cómputo  
    - Servicios relacionados  
* **Presencia**  
    - [Twitter](https://twitter.com/Nearsoft)  
    - [Facebook](https://www.facebook.com/Nearsoft/)  
    - [LinkedIn](https://www.linkedin.com/company/nearsoft/)  
* **Ofertas laborales**  
[Become a Nearsoftian! • Nearsoft](https://nearsoft.com/join-us/)  
* **Blog de Ingeniería**  
[Closer is Better • Nearsoft](https://nearsoft.com/blog/)  
* **Tecnologías**  
    - Java  
    - Microsoft .NET  
    - JavaScript  
    - Ruby  
    - Swift  
    - Python  
    - GO  
    - Elixir  
    - Scala  
    - Clojure  
    - Node  
    - DevOps  
    - React  
    - Full Stack  
    - Test  
    - WordPress  

## **Tiempo Development**
* **Logotipo**  
![Tiempo Development](imagenes/TiempoDevelopment.png)  
* **Sitio web**  
[Tiempo Dev Is Now a 3Pillar Global Company](https://www.tiempodev.com)  
* **Ubicación**  
    - Guadalajara  
      [C. Justo Sierra 2464, Ladron De Guevara, 44600 Guadalajara, Jal.](https://goo.gl/maps/tUXn49WqhhHV5UjW9)  
    - Monterrey    
      [Valparaíso 49, Alta Vista, 64840 Monterrey, N.L.](https://goo.gl/maps/8CfS23iFps4JkJGN8)  
    - Sonora  
      [Blvd. Antonio Quiroga 547-Local 23, El Llano, Quinta Emilia, 83210 Hermosillo, Son.](https://goo.gl/maps/2msivfVPNVepoNMA6)  
* **Acerca de**  
El equipo de liderazgo de Time continúa impulsando su éxito galardonado.  
Tiempo es ampliamente reconocida como una de las principales empresas de ingeniería de software de EE. UU. Poniendo a trabajar recursos de ingeniería nearshore y equipos de alto desempeño con un enfoque incansable en los resultados del cliente, Tiempo diseña, construye e implementa software que mejora la vida.  
Tiempo tiene su sede en Tempe, Arizona, con cuatro instalaciones de desarrollo de software de clase mundial en México. Tiempo ha sido reconocida anualmente por Inc. Magazine como una de las empresas privadas de más rápido crecimiento en Estados Unidos.  
* **Servicios**  
    - Análisis técnico  
    - Arquitectura de software  
    - Diseño UI/UX  
    - Gestión de productos  
    - Desarrollo de aplicaciones web  
    - Desarrollo de aplicaciones móviles  
    - Integración de aplicaciones / API  
    - Mantenimiento y soporte de aplicaciones  
    - Pruebas y aseguramiento de la calidad  
* **Presencia**  
    - [Facebook](https://www.facebook.com/TiempoDevelopmentLLC/)  
    - [LinkedIn](https://mx.linkedin.com/company/tiempo-development)  
* **Ofertas laborales**  
[Dev Jobs at Tiempo Development - Challenge, Learn & Grow With Us](https://www.tiempodev.com/careers/)  
* **Blog de Ingeniería**  
[Software Expertise Articles & Nearshore Advantages - Tiempo](https://www.tiempodev.com/blog/)  
* **Tecnologías**  
    - Java  
    - Kotlin  
    - Objetive-C  
    - Xamarin  
    - Swift  
    - Adobe PhoneGap  
    - React  
    - CSS  
    - HTML 5  
    - JavaScript  
    - AngularJS  
    - ReacUS  
    - Microsoft .NET  
    - Python  
    - Flask  
    - Django  
    - NodeJS  
    - Express JS  
    - PHP  
    - Laravel  
    - C/C++  
    - Amazon Web Service  
    - Azure  
    - Google Cloud  
    - Lamp  
    - Falcon  
    - Tornado  
    - Apache Kafka  
    - Kubernetes  
    - Spring Boot  
    - Docker  
    - Eureka  
    - Registrator  
    - Prana  
    - Zookeeper  
    - RabbitMQ  
    - JMS  
    - Rest  
    - RPC  
    - SOAP  
    - AWS API Gateway  
    - Azure API Gateway  
    - MySQL  
    - MariaDB  
    - Oracle  
    - PostgreSQL  
    - Microsoft SQL Server  
    - Mongo  
    - Cassandra  
    - Redis  
    - Y más  

## **MagmaLabs**
* **Logotipo**  
![MagmaLabs](imagenes/MagmaLabs.svg)  
* **Sitio web**  
[MagmaLabs - Exceptional Custom Software](https://www.magmalabs.io)  
* **Ubicación**  
    - Colima  
      [AV. Constitución 2035 Santa Gertrudis Colima  COL  México C.P: 28017](https://goo.gl/maps/sxJVz9rPr1BJ2YBv9)  
    - Aguascalientes  
      [Aguascalientes Sierra Pintada 112 Bosques del Prado Sur Aguascalientes  Ags  México C.P: 20130](https://goo.gl/maps/saCUFEZj6zoPnAA4A)  
* **Acerca de**  
Somos una agencia de desarrollo de software estadounidense con operaciones en México y otras partes de América Latina. Desde el inicio, nuestra misión ha sido impulsar la innovación mientras desarrollamos talento humano de clase mundial.  
Durante los últimos diez años, hemos trabajado en más de cien proyectos para clientes de todo el mundo y nos hemos ganado una reputación notable como especialistas en comercio electrónico.  
Obie Fernandez (uno de los expertos en Ruby on Rails más conocidos del mundo) se unió a nosotros como socio y consultor jefe en enero de 2020 cuando nos fusionamos con su firma Kickass Partners.  
* **Servicios**  
    - Diseño Web  
    - Desarrollo de software personalizado  
* **Presencia**  
    - [Twitter](https://twitter.com/wearemagmalabs)  
    - [Facebook](https://www.facebook.com/magmalabsio/)  
    - [LinkedIn](https://mx.linkedin.com/company/magmalabs)  
* **Ofertas laborales**  
[BambooHR](https://magmalabs.bamboohr.com/jobs/)  
* **Blog de Ingeniería**  
[MagmaLabs Technical Blog - MagmaLabs technical blog and discussions](http://blog.magmalabs.io)  
* **Tecnologías**  
    - Ruby  
    - React  
    - React Native  
    - Node JS    

## **IcaliaLabs**
* **Logotipo**  
![IcaliaLabs](imagenes/IcaliaLabs.png)  
* **Sitio web**  
[Icalia Labs - High-end Custom Software Development Services](https://www.icalialabs.com)  
* **Ubicación**  
    - Monterrey  
      [Av. Eugenio Garza Sada 3820 Más Palomas (Valle de Santiago) 64860 Monterrey, Nuevo León](https://goo.gl/maps/m3hHarvqVYgL9iGEA)  
* **Acerca de**  
En Icalia Labs transformamos negocios con soluciones digitales poderosas y adaptables que satisfacen las necesidades de hoy y desbloquean las oportunidades del mañana.  
* **Servicios**  
    - Desarrollo de software personalizado  
    - Desarrollo web y móvil  
    - Modernización de software heredado  
    - Diseño de producto Sprint  
    - Taller de diseño de producto  
    - Evaluación de tecnología  
    - Estrategia de innnovación  
    - Dotación de personal remota  
* **Presencia**  
    - [GitHub](https://github.com/IcaliaLabs)  
    - [dribbble](https://dribbble.com/icalialabs/members)  
    - [LinkedIn](https://www.linkedin.com/company/icalia-labs/)  
    - [Clutch](https://clutch.co/profile/icalia-labs)  
* **Ofertas laborales**  
[Icalia Labs - Software Engineer](https://www.icalialabs.com/work-design-sprint-custom-software-legacy-software)  
[Icalia Labs - Digital Designer](https://www.icalialabs.com/open-positions/digital-designer)  
* **Blog de Ingeniería**  
[Icalia's Noteworthy Tips, Tricks and News - Software Development Blog](https://www.icalialabs.com/blog-software-development-tips-and-news)  
* **Tecnologías**  
    - Figma  
    - Sketch  
    - Adobe Illustrator  
    - Adobe Photoshop  
    - Adobe XD
    - InVision
    - CSS  
    - HTML  
    - Java  
    - Ruby on Rails  
    - Microsoft .NET  
    - C++  
    - JavaScript  
    - SDLC  
    - JQuery  
    - Sass  
    - Less  
    - Furatto  
    - BootStrap  
    - Foundation  

## **Tacit knowledge**
* **Logotipo**  
![Tacit knowledge](imagenes/TacitKnowledge.png)  
* **Sitio web**  
[Home - Tacit Knowledge](https://www.tacitknowledge.com)  
* **Ubicación**  
    - Guadalajara  
      [Av. Américas 1545, Piso 7 Colonia Providencia, Guadalajara Jal, 44630](https://goo.gl/maps/AFYGitj8yDeWmSBh6)  
* **Acerca de**  
Tacit Knowledge crea, integra y respalda software empresarial para marcas globales. El enfoque principal de Tacit es el comercio digital y hemos ganado varios premios por nuestro trabajo en esta área. Nuestra experiencia internacional se extiende a implementaciones dentro del comercio móvil, comercio social y de gran escala. Nuestro historial de clase mundial se deriva de la especialización en tecnologías clave y equipos de campo con amplia experiencia en el dominio. También ofrecemos servicios de consultoría en selección de productos, diseño creativo y UX, coaching ágil, estabilización de sistemas y ajuste del rendimiento. Y, por último, ofrecemos soporte de aplicaciones "follow the sun" las 24 horas del día, los 7 días de la semana, monitoreo y alertas de clase mundial y gestión de incidentes.  
* **Servicios**  
    - Consultoría de software  
    - Consultoría Ecommerce  
    - Comercio digital  
    - Servicios gestionados  
    - Peak Ready performance  
    - Comercio SAP  
    - NCommerce    
* **Presencia**  
    - [Twitter](https://twitter.com/tacitknowledge)  
    - [LinkedIn](https://www.linkedin.com/company/tacit-knowledge)  
* **Ofertas laborales**  
[Careers - Tacit Knowledge](https://www.tacitknowledge.com/careers)  
* **Blog de Ingeniería**  
[Thoughts - Tacit Knowledge](https://www.tacitknowledge.com/thoughts/)  
* **Tecnologías**  
    - AWS  
    - GCP  
    - Azure  
    - Docker  
    - Kubernetes  
    - HTTP  
    - Nginx  
    - Apache  
    - MySQL  
    - MSSQL  
    - PostgreSQL  
    - Ruby  
    - Python  
    - Make  
    - Ant  
    - Maven  
    - Rake  

## **densitylabs**
* **Logotipo**  
![densitylabs](imagenes/densitylabs.svg)  
* **Sitio web**  
[Density Labs | Home](https://densitylabs.io)  
* **Ubicación**  
    - Guadalajara  
      [Av Adolfo López Mateos Nte # 95 Col. Providencia CP 44648. Guadalajara, Jalisco. México](https://goo.gl/maps/TDtAHXpXeuGcKPCh9)  
    - Zapopan  
      [Claveles # 566 Col. Los Girasoles CP 45136. Zapopan, Jalisco. México](https://goo.gl/maps/2sH7X3xZgFzzu5xh8)  
* **Acerca de**  
Density Labs es una empresa de desarrollo de productos de tecnología que se especializa en el desarrollo rápido de aplicaciones de software para móviles, web, SaaS y empresariales.  
* **Servicios**  
    - Desarrollo de software  
    - Control de calidad y automatización  
    - DevOps  
    - Gestión de proyectos  
    - Diseño UI/UX  
* **Presencia**  
    - [Twitter](https://twitter.com/densitylabs)  
    - [LinkedIn](https://www.linkedin.com/company/densitylabs)  
    - [Facebook](https://www.facebook.com/densitylabs)  
    - [Youtube](https://www.youtube.com/channel/UCk5QUrs2j63QpC8JjQ8yr-A)  
* **Ofertas laborales**  
[Density Labs | Careers](https://densitylabs.io/careers)  
* **Blog de Ingeniería**  
[Density Labs | Blog](https://densitylabs.io/blog)  
* **Tecnologías**  
    - Ruby  
    - React  
    - Node  
    - JavaScript  
    - HTML  
    - CSS  
    - SASS  
    - Redux  
    - API UI/UX  

## **iTexico**
* **Logotipo**  
![itexico](imagenes/itexico.jpg)  
* **Sitio web**  
[Nearshore Software Development Services Company | Improving Nearshore](https://www.itexico.com)  
* **Ubicación**  
    - Guadalajara  
      [Av. Patria 888, Loma Real, 45129 Guadalajara, México](https://g.page/improving-guadalajara?share)  
    - Aguascalientes  
      [Ciencia y Tecnología 106, Parque Industrial Tecnopolo San José Pocitos, 20328 Aguascalientes, México](https://goo.gl/maps/QgWs3aee7pv2tj798)  
* **Acerca de**  
La mejora ha ayudado a más de 300 clientes a reducir los riesgos de subcontratar el desarrollo de software aprovechando un modelo de negocio Nearshore +, integrando las mejores prácticas de los equipos ágiles en la forma en que necesita la innovación digital e implementando las mejores prácticas de prestación de servicios empresariales.  
* **Servicios**  
    - Sprints de diseño  
    - Talleres de descubrimiento de productos  
    - Diseño UI/UX  
    - Gestión de productos  
    - Talleres de descubrimiento de ingeiería  
    - Desarrollo de software especializado  
    - Modernizacion de producto  
    - Prueba de software manual  
    - Pruebas automatizadas  
    - Talleres de descubrimiento móvil  
    - Evaluaciones móviles  
    - Desarrollo de software móvil  
    - DevOps móvil  
    - Estrategia y optimización de la nube  
    - Migración y gestión de la nube  
    - DevOps como servicio  
    - Seguridad  
    - Sprints de diseño de IA  
    - Ingeniería y visualización de datos  
    - Automatización de procesos  
    - Aprendizaje automático  
    - Analítica predictiva  
* **Presencia**  
    - [Twitter](https://twitter.com/improvingmx)  
    - [LinkedIn](https://www.linkedin.com/company/795788)  
    - [Facebook](https://www.facebook.com/itexico)  
    - [Youtube](https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw)  
* **Ofertas laborales**  
[Careers | Improving Nearshore](https://www.itexico.com/careers)  
* **Blog de Ingeniería**  
[Nearshore + Outsourcing Blog | iTexico](https://www.itexico.com/blog)  
* **Tecnologías**  
    - Microsoft .NET  
    - JavaScript  
    - Python  
    - SQL  
    - Kafka  
    - AWS Kinesis  
    - Google CLoud  
    - Azure  
    - Flink  
    - ETL  
    - Backend APis  
    - Jira  
    - Tricentis Qtest  
    - GitLab  
    - Jenkins  
    - MVC  
    - MVP  
    - MVVM  
    - Objetive-C  
    - Swift  
    - JavaScript  
    - HTML  
    - CSS  
    - TypeScript  
    - Java  
    - PHP  
    
## **Tango**
* **Logotipo**  
![Tango](imagenes/tango.svg)  
* **Sitio web**  
[Product Development and Staff Augmentation Consultancy](https://tango.io)  
* **Ubicación**  
    - Colima  
      [Av La Paz 51, Sta Bárbara, 28017 Colima, Col.](https://goo.gl/maps/QtC3nESKnxq8o4Xk6)  
* **Acerca de**  
Equipos impulsados culturalmente con pasión por el producto  
Tango (anteriormente TangoSource), ayuda a empresas emergentes, medianas y empresariales innovadoras a desarrollar productos digitales impactantes a través de asociaciones apasionadas. ¡Hemos ayudado a más de 100 empresas a desarrollar y escalar sus productos de software desde 2009! "  
* **Servicios**  
    - Desarrollo de software  
    - Ingenieros en pruebas  
    - Analistas de negocios  
    - Gerentes de proyectos  
    - Scrum Masters  
    - DevOps  
    - Líderes de producto  
    - Producto mínimo viable  
    - Aumento del personal  
    - Resolución de deuda técnica  
    - Estrategia de producto  
    - Escalabilidad y aceleración del producto  
* **Presencia**  
    - [Twitter](https://twitter.com/tango_io)  
    - [LinkedIn](https://www.linkedin.com/company/tango-io/)  
    - [Facebook](https://www.facebook.com/GoTango.io/)  
    - [Instagram](https://www.instagram.com/tangodotio/)  
* **Ofertas laborales**  
[Tango](https://jobs.lever.co/tango)  
* **Blog de Ingeniería**  
[Tango: Product Development & Hiring Solutions – Tango is a software development agency. We help innovative startups to develop and scale impactful digital products.](https://blog.tango.io)  
* **Tecnologías**  
    - DevOps  
    - Jenkins 
    - Terraform  
    - Docker  
    - Heroku  
    - WebDev  
    - JavaScript  
    - React  
    - HTML  
    - CSS  
    - Ruby on Rails  
    - Node  
    - React  
    - Native  
    - Java  
    - Kotlin  
    - Swift  
    - Ionic  
    - Objective C  
    - SaaS  
    - Product Requirement Analysis  
    - Product Backlog Planning  
    - Release Forecasting  
    - MVP Building  
    - UX/UI  
    - Adobe XD  
    - Photoshop Illustrator  
    - Sketch  
    - Invision  
    - Shopify  
    - Stripe  
    - Solidus  
    - Magento  
    - Serverless  
    - AWS Lambda  
    - Google Cloud Functions  
    - Serverless Framework  
    - GuardDuty  
    - AWS Inspector  
    - AWS Shield  
    - Cloudwatch  

## **Unosquare**
* **Logotipo**  
![unosquare](imagenes/unosquare.svg)  
* **Sitio web**  
[Digital Transformation Services | Agile Staffing Solutions | Unosquare](https://www.unosquare.com)  
* **Ubicación**  
    - Guadalajara  
      [Av. Américas 1536 1A Col. Country Club C.P. 44637 Guadalajara, JAL México](https://goo.gl/maps/eqMVaftkv55hfDpKA)  
* **Acerca de**  
NUESTRA MISIÓN  
Unosquare, en su esencia, tiene una misión impulsada por tres propósitos rectores para cada individuo que forma parte de la organización:  
PERSONAL  
Fundamentalmente, la empresa de desarrollo de software ágil se creó entendiendo que, en nuestro núcleo, no somos más que un grupo de personas que empujan nuestros límites personales para hacer el mejor trabajo que hemos hecho. Un buen ambiente de trabajo es clave para la creatividad, la colaboración y, en última instancia, el éxito.  
PROFESIONAL  
Nos esforzamos por crear un entorno donde nuestro personal pueda disfrutar de la base cultural y las relaciones personales que han creado a lo largo de su tiempo aquí. También queremos colaborar en proyectos que desafíen nuestras capacidades técnicas y que tengan un nivel de complejidad que impulse el crecimiento profesional del individuo y del grupo, el emprendimiento y el comportamiento auto didáctico.  
SOCIAL  
Unosquare está aquí para crear un legado. Tenemos el compromiso de generar bienestar en todas las geografías en las que tenemos presencia. Nuestra organización ha sido bendecida con la oportunidad de impactar sustancialmente el espíritu en el que continuamos desarrollando la empresa. Creemos en un esfuerzo global hacia la igualdad de oportunidades.  
* **Servicios**  
    - Desarrollo ágil de software  
    - Servicios de desarrollo Nearshore  
    - Consultoría de proyectos de tecnología  
    - Estrategias de transformación digital  
    - Aumento ágil distribuido  
    - Consultoría de proyectos de tecnología  
    - Transformación digital   
* **Presencia**  
    - [Twitter](https://twitter.com/unosquare)  
    - [LinkedIn](https://www.linkedin.com/company/293703/)  
    - [Facebook](https://www.facebook.com/unosquare/)  
* **Ofertas laborales**  
[Unosquare Careers](https://people.unosquare.com/#welcome)  
* **Blog de Ingeniería**  
[Software Development Blog | Digital Transformation Blog](https://blog.unosquare.com)  
* **Tecnologías**  
    - Chef  
    - Hadoop  
    - MySQL  
    - Postgres  
    - Elastic Search  
    - VMware vSphere  
    - Juniper  
    - HP ISS  
    - HP 3Par  
    - F5 LTM  
    - Nagios  
    - Cacti  
    - SQL  
    - PL/SQL  
    - React/Redux CSS  
    - PHP  
    - Ruby  
    - Python  
    - Rust  
    - Node   
    - .NET  
    - Git  
    - SVN  
    - Docker  
    - Zephyr  

## **Abraxas Intelligence**
* **Logotipo**  
![Abraxas](imagenes/abraxas.svg)  
* **Sitio web**  
[Abraxas Intelligence | Machine Learning, AI solutions and Data Science](https://www.abraxasintelligence.com)  
* **Ubicación**  
    - CDMX  
      [Donato Guerra 9, Tizapán San Angel, Del. Álvaro Obregón CP 05000 CDMX, México](https://goo.gl/maps/KHJ8m4SEy6aTH1z16)  
* **Acerca de**  
Brindamos Machine Learning, soluciones de inteligencia artificial y productos de ciencia de datos para mejorar los procesos, aprovechar las oportunidades comerciales y agregar un valor significativo a las organizaciones.  
* **Servicios**  
    - ML personalizado  
    - Web de optimización de precios  
    - Producto de mapa de inteligencia  
    - Producto de visión artificial  
* **Presencia**  
    - [GitHub](https://github.com/Grupo-Abraxas)  
    - [LinkedIn](https://www.linkedin.com/company/abraxas-group/)  
* **Ofertas laborales**  
[Abraxas Ventures | Careers](https://www.abraxas.ventures/careers)  
* **Blog de Ingeniería**  
[Abraxas Ventures | Life at Abraxas](https://www.abraxas.ventures/life-at-abraxas)  
* **Tecnologías**  
    - React  
    - HTML  
    - CSS  
    - JavaScript  
    - Python  
    - SQL  
    - Kafka  
    - AWS Kinesis  
    - Google CLoud  
    - Azure  
    - Flink  
    - Docker  
    - Kubernetes  
    - Nginx  
    - Apache  
    - MySQL  
    - MSSQL  

## **Scio Consulting**
* **Logotipo**  
![Scio](imagenes/Scio.png)  
* **Sitio web**  
[Scio México](http://www.scio.com.mx)  
* **Ubicación**  
    - Morelia  
      [Av Las Cañadas 501-Int. 230, Tres Marías, 58254 Morelia, Mich.](https://goo.gl/maps/8QoeZZWi8LLMvTrh9)  
* **Acerca de**  
Nos dedicamos al desarrollo de soluciones web, en la nube y SaaS, así como también soporte.
Trabajamos con nuestros clientes para construir y operar aplicaciones y servicios confiables. Adoptamos un enfoque de ciclo de vida completo, asociándonos con nuestros clientes desde la conceptualización, pasando por el desarrollo, hasta el mantenimiento, soporte y DevOps en curso.  
* **Servicios**  
    - Aplicaciones Web  
    - Aplicaciones Móviles  
    - Consultoría Tecnológica  
    - Servicios en la Nuve  
    - Desarrollo de aplicaciones de Negocio  
    - Soluciones de Software  
* **Presencia**  
    - [Twitter](https://twitter.com/sciomx)  
    - [Facebook](https://www.facebook.com/ScioMx)  
* **Ofertas laborales**  
[Vacantes - Scio México](https://www.scio.com.mx/trabaja-scio-mexico/vacantes/)  
* **Blog de Ingeniería**  
[Blog - Scio México](https://www.scio.com.mx/blog/)  
* **Tecnologías**  
    - React Native  
    - Node  
    - Java  
    - HTML  
    - Kotlin  
    - Swift  
    - Ruby on Rails  
    - JavaScript  
    - React  
    - CSS  

## **Indigo**
* **Logotipo**  
![Indigo](imagenes/indigo.svg)  
* **Sitio web**  
[Indigo Smart Software Development](https://www.dsindigo.com)  
* **Ubicación**  
    - CDMX  
      [Luis Braile 27, Del. Benito Juárez, Mexico City. 03660](https://g.page/DSIndigo?share)  
* **Acerca de**  
Ubicada en la Ciudad de México, Indigo es una boutique de software especializada en el diseño y desarrollo de aplicaciones personalizadas.  
Con más de 8 años de experiencia y un equipo de aproximadamente 80 personas, nuestro objetivo es ofrecer excelentes aplicaciones con el mayor valor posible para nuestros clientes.  
* **Servicios**  
    - Desarrollo de software a medida  
    - Dotación de personal cerca de la costa  
* **Presencia**  
    - [LinkedIn](https://mx.linkedin.com/company/indigo-smart-software-development)  
* **Ofertas laborales**  
[Indigo Smart Software Development](https://www.linkedin.com/jobs/indigo-smart-software-development-jobs-worldwide?position=1&pageNum=0)  
* **Blog de Ingeniería**  
[Indigo Smart Software Development | Ln](https://mx.linkedin.com/company/indigo-smart-software-development)  
* **Tecnologías**  
    - React  
    - .NET  
    - JAVA  
    - PYTHON  
    - C#  
    - KOTLIN  

## **Svitla Systems**
* **Logotipo**  
![Svitla](imagenes/svitla.png)  
* **Sitio web**  
[Software Development Company | Svitla Systems](https://svitla.com)  
* **Ubicación**  
    - CDMX  
      [C. Colonias #221-piso 4, Col Americana, Americana, 44160 Guadalajara, Jal.](https://goo.gl/maps/8t8Y4PgDGtNvbZZYA)  
* **Acerca de**  
Es su conducto hacia las innovaciones tecnológicas más vanguardistas, desde Web y dispositivos móviles hasta Big Data e Internet de las cosas. Entregamos un valor incomparable a nuestros clientes.  
* **Servicios**  
    - Servicio de consultoría  
    - Extensión de equipo administrado  
    - AgileSquads (Escuadrones ágiles)  
* **Presencia**  
    - [Facebook](https://www.facebook.com/SvitlaSystems)  
    - [LinkedIn](https://www.linkedin.com/company/svitla-systems-inc-/?trk=biz-companies-cym)  
    - [Youtube](https://www.youtube.com/channel/UC1nu2LV4_08GoZThHEindWA)  
    - [Instagram](https://www.instagram.com/svitlasystems)  
    - [Twitter](https://twitter.com/SvitlaSystemsIn)  
* **Ofertas laborales**  
[Svitla Systems Careers](https://svitla.com/career)  
* **Blog de Ingeniería**  
[Svitla Systems: Blog on Software Development News](https://svitla.com/blog)  
* **Tecnologías**  
    - Microsft SQL Server  
    - SSIS  
    - Azure  
    - Data  
    - Factory  
    - SQL  
    - Stored  
    - SQL Server/T-SQL  
    - Power BI Ecosystem  
    - Data warehouse environment  
    - Python  
    - Ruby  
    - C#  

## **Proscai**
* **Logotipo**  
![Proscai](imagenes/proscai.jpg)  
* **Sitio web**  
[Proscai - Soluciones ERP y Punto de Venta para México](https://www.proscai.com)  
* **Ubicación**  
    - Ciudad de México  
      [Presidente Masaryk 101-601 Ciudad de México, Polanco México, 11560](https://goo.gl/maps/z7tPpF7UGs4D2Ryq5)  
* **Acerca de**  
Nuestra empresa nace dentro la industria con el propósito de desarrollar aplicaciones de tecnología que faciliten el trabajo de las personas, logrando que las empresas lleguen a su máximo potencial.  
* **Servicios**  
    - Desarrollos a la medida  
    - Implementación  
    - Capacitación  
    - Consultoría  
* **Presencia**  
    - [Foro Proscai](https://ayuda.proscai.com/hc/es-419/community/topics)  
* **Ofertas laborales**  
[Proscai - Forma parte de nuestro equipo](https://www.proscai.com/careers)  
* **Blog de Ingeniería**  
[Proscai Blog – Artículos y noticias referentes a la industria retail.](https://blog.proscai.com)  
* **Tecnologías**  
    - UX  
    - Swift  
    - APIs tipo REST y GraphQL  
    - React  
    - Node  
    - REST  
    - GraphQL  
    - Git  
    - HTML  
    - CSS  
    - .NET  
    - infraestructura en la nube  
    - Servicios de backend  
    - SaaS  
    - MySQL  
    - PostgreSQL  
    - NoSQL  
    - DevOps  
    - RabbitMQ  
    - Redis  

## **Onephase**
* **Logotipo**  
![Onephase](imagenes/onephase.svg)  
* **Sitio web**  
[Onephase - Home](https://onephase.com)  
* **Ubicación**  
    - Monterrey  
      [Av. Revolución 4020-Local 6, Colonial La Silla, 64860 Monterrey, N.L.](https://goo.gl/maps/ej2wycaBh6a9cnd6)  
* **Acerca de**  
Onephase crea una alianza estratégica que impulsa el crecimiento de la tecnología como nuestro principal impulsor.  
Transformaremos la forma en que se brindan los servicios de TI, trabajaremos en estrecha colaboración con la industria para brindar más beneficios a nuestras ofertas, siempre manteniendo nuestro enfoque centrado en el cliente.  
* **Servicios**  
    - Consultoría digital  
    - Control y prueba de calidad de software  
    - Desarrollo de software  
    - Diseño UI / UX  
    - Servicios en la nube  
    - Soluciones de comercio electrónico  
    - Dotación de personal cerca de la costa  
    - Big Data  
    - Servicios de RV / RA  
* **Presencia**  
    - [Instagram](https://www.instagram.com/onephasellc/)  
    - [Facebook](https://www.facebook.com/OnephaseLLC/)  
    - [Twitter](https://twitter.com/OnePhaseLLC)  
    - [LinkedIn](https://www.linkedin.com/in/onephasellc/)  
* **Ofertas laborales**  
[Onephase | Ln](https://www.linkedin.com/jobs/onephase-jobs-worldwide?f_C=15196098&trk=top-card_top-card-primary-button-top-card-primary-cta&position=1&pageNum=0)  
* **Blog de Ingeniería**  
[OnePhase - Insight](https://onephase.com/insight)  
* **Tecnologías**  
    - UX  
    - React  
    - Azure  
    - Ionic  
    - Xamarin  
    - React  
    - Swift  
    - AWS  
    - Docker  
    - PostgreSQL  
    - SQL  
    - Angular  
    - Django  
    - NodeJS  
    - .NET  
    - Data Base  
    - MySQL  
    - Unity  
    - ASP  
    - Rails  
    - Laravel  
    - MongoDB  
    - Redis  
    - Cloud Services  
    - Jenkinks  
    - Heroku  

## **Grow IT Consulting**
* **Logotipo**  
![Grow](imagenes/growit.png)  
* **Sitio web**  
[Grow – Empower Dynamics](http://www.grw.com.mx)  
* **Ubicación**  
    - CDMX  
      [San Antonio 120 Int. 101 y 102 Col. Napoles, Delegación Benito Juarez CDMX, C.P. 03810](https://goo.gl/maps/6aiFzfpuojCFtG4S9)  
* **Acerca de**  
Onephase crea una alianza estratégica que impulsa el crecimiento de la tecnología como nuestro principal impulsor.  
Transformaremos la forma en que se brindan los servicios de TI, trabajaremos en estrecha colaboración con la industria para brindar más beneficios a nuestras ofertas, siempre manteniendo nuestro enfoque centrado en el cliente.  
* **Servicios**  
    - Acceso a una serie de soluciones pre-construidas  
    - Acceso a la experiencia (técnica y funcional) derivada de las múltiples implantaciones  
    - Optimizar la inversión del cliente final a través de soluciones complementarias  
    - Reducción de los costos fijos que implica el invertir en la creación o crecimiento de un área de desarrollo tecnológico propia.  
* **Presencia**  
    - [Instagram](https://www.instagram.com/grow_it_mx/)  
    - [Facebook](https://www.facebook.com/growitc)  
    - [LinkedIn](https://www.linkedin.com/company/grow-it-consulting)  
* **Ofertas laborales**  
[Bolsa de Trabajo – Grow](http://www.grw.com.mx/bolsa-de-trabajo/)  
* **Blog de Ingeniería**  
[OnePhase - Insight](http://www.grw.com.mx/blog/)  
* **Tecnologías**  
    - scrum  
    - Agile Unified Process  
    - PYTHON  
    - C#  
    - Uso de base de datos intermedio- avanzado (Stored Procedures, Vistas, Triggers , POO)  
    - .NET  
    - JAVA  

## **Intelimetrica**
* **Logotipo**  
![Intelimetrica](imagenes/inteli.png)  
* **Sitio web**  
[Intelimétrica](https://www.intelimetrica.com)  
* **Ubicación**  
    - CDMX  
      [Mariano Escobedo #748, P4 Col. Anzures Alcaldía Miguel Hidalgo CDMX 11590](https://goo.gl/maps/4owgwH5dHSauUTCN6)  
* **Acerca de**  
Estamos dedicados y comprometidos a acelerar la innovación empresarial a través de tecnología, ingeniería y diseño superiores.  
Somos una empresa de desarrollo de soluciones que aprovecha las mejores tecnologías para ayudar a los clientes a reinventarse.  
* **Servicios**  
    - IA y ML  
    - Ingeniería de software  
    - Diseño UX  
* **Presencia**  
    - [Twitter](https://twitter.com/Intelimetrica)  
    - [Facebook](https://www.facebook.com/intelimetrica)  
    - [LinkedIn](https://www.linkedin.com/company/intelimétrica/)  
    - [GitHub](https://github.com/Intelimetrica)  
* **Ofertas laborales**  
[Intelimétrica - Careers](https://www.intelimetrica.com/careers)  
* **Blog de Ingeniería**  
[Intelimétrica | blog](https://www.linkedin.com/company/intelimétrica/)  
* **Tecnologías**  
    - Java  
    - Elixir 
    - JavaScript  
    - Ruby  
    - Swift  
    - Python  
    - PostgreSQL  
    - Computación distribuida  
    - Canalizaciones ETL de programación paralela  
    - Modelado de datos  
    - Computación en la nube e infraestructura de datos
    - web scraping  
    - Flask  
    - FastAPI  
    - Phoenix  
    - Git  
    - SOLID  
    - KISS  
    - DRY  
    - YAGNI  

## **GFT**
* **Logotipo**  
![GFT](imagenes/GFT.svg)  
* **Sitio web**  
[Soluciones de IT para Industria y Compañías financieras | Soluciones de IT para Industria y Compañías financieras](https://www.gft.com/mx/es/index/)  
* **Ubicación**  
    - CDMX  
      [Gobernador Agustín Vicente Eguia 46, San Miguel Chapultepec I Secc, Miguel Hidalgo, 11850 Ciudad de México, CDMX, México](https://goo.gl/maps/n3rgmnyFxWvWta7V8)  
* **Acerca de**  
Comenzamos como una pequeña empresa en la Selva Negra alemana y hemos crecido hasta convertirnos en especialistas del sector financiero a nivel internacional.  
Somos pioneros en ofrecer servicios nearshore desde 2001 y contamos con un equipo de 7.000 colaboradores en más de 15 países.  
Sin embargo,  lo más importante  es que seguimos siendo humanos. Somos fieles a nuestros principios porque son muy importantes para nosotros. Nos apasiona la tecnología y nos encanta descubrir cosas nuevas. Combinamos una praxis según las tendencias consolidadas del sector junto con nuevas formas de pensar, integrando ambos conceptos en una solución que te permita destacar frente a tus competidores.  
* **Servicios**  
    - Inteligencia Artificial  
    - Blockchain y DLT  
    - CLOUD  
    - Análisis de datos  
    - Greencoding  
* **Presencia**  
    - [Twitter](https://twitter.com/gft_mx)  
    - [Facebook](https://www.facebook.com/gft.mex)  
    - [LinkedIn](https://www.linkedin.com/company/gft-group)  
    - [Instagram](https://www.instagram.com/gft_tech/)  
    - [Youtube](https://www.youtube.com/user/gftgroup)  
* **Ofertas laborales**  
[GFT - JOBS](https://jobs.gft.com/Mexico/go/mexico/4412401/)  
* **Blog de Ingeniería**  
[GFT España](https://blog.gft.com/es/)  
* **Tecnologías**  
    - HTML5  
    - CSS3  
    - JavaScript  
    - ANGULAR  
    - BOOTSTRAP 4  
    - Java  
    - Swift  
    - Microservicios  
    - Spring 5  
    - Spring boot  
    - Spring cloud  
    - Git  
    - Junit  
    - Mockito  
    - Assertj
    - Pruebas de Integración y Cobertura  
    - WebServices Soap y Res  
    - MVC  
    - MVVM  
    - Coordinator  
    - Singleton  

## **Luxoft**
* **Logotipo**  
![Luxoft](imagenes/Luxoft.svg)  
* **Sitio web**  
[Luxoft | Digital Strategy, Consulting and Engineering at Scale](https://www.luxoft.com)  
* **Ubicación**  
    - Guadalajara  
      [Amado Nervo 1717, Cigatam, 45054 Zapopan, Jal., México](https://goo.gl/maps/sa6CY2xiDG7r6qF99)  
* **Acerca de**  
DXC Luxoft capacita a los líderes empresariales con capacidades mejoradas de análisis e ingeniería de software que estabilizan a las empresas y las ayudan a prosperar en mercados complejos y cambiantes.  
* **Servicios**  
    - Datos a conocimientos  
    - Todo automatizado  
    - Modernización  
    - Transformación y agilidad  
    - Cosas y lugares inteligentes  
* **Presencia**  
    - [Twitter](http://twitter.com/Luxoft)  
    - [Facebook](https://www.facebook.com/Luxoft)  
    - [LinkedIn](http://www.linkedin.com/companies/luxoft)  
    - [Youtube](http://www.youtube.com/channel/UCDtOIqWxKHTdtmVi8yr_D7Q)  
* **Ofertas laborales**  
[Luxoft | Digital Strategy, Consulting and Engineering at Scale](https://www.luxoft.com/#careers)  
* **Blog de Ingeniería**  
[Behavioral Archetypes Toolkit for Assessing Customer Persona](https://www.luxoft.com/blog/annaho/behavioral-archetypes-toolkit-for-assessing-customer-persona/)  
* **Tecnologías**  
    - Java  
    - PMO  
    - QA  
    - C++  
    - Python  
    - Git  
    - Jira  
    - Test Rail  
    - Nexus  
    - Artifactory  
    - Terraform  
    - Ansible  
    - Jenkins  
    - TeamCity  
    - Docker  
    - GCP  
    - Azure  
    - AWS  
    - Shell  
    - Versioning control systems  

## **AMK Technologies**
* **Logotipo**  
![AMK](imagenes/AMK.png)  
* **Sitio web**  
[AMK Techonologies](https://amk-technologies.com)  
* **Ubicación**  
    - CDMX  
      [C. Tlacoquemecatl 21, Del Valle, Benito Juárez, 03200 Ciudad de México, CDMX, México](https://goo.gl/maps/ukf5DytVKtpMJ8gd9)  
Somos una unidad horizontal que trabaja con los diferentes mercados verticales a la hora de definir, diseñar y operar un servicio de Staffing de TI.    
* **Servicios**  
    - Disrupcion digital  
    - Metodología Ágil  
    - DevOps  
* **Presencia**  
    - [Twitter](https://twitter.com/search?q=%40AmkTechnologies&src=typd&lang=es)  
    - [Facebook](https://www.facebook.com/AMKTechnologiesMx/)  
    - [LinkedIn](https://mx.linkedin.com/company/amk-technologies)  
    - [Instagram](https://www.instagram.com/amktechnologies/)  
    - [WhatsApp](https://api.whatsapp.com/send?phone=525560664675)  
* **Ofertas laborales**  
[AMK Techonologies](https://amk-technologies.com/#people)  
* **Blog de Ingeniería**  
[AMK T](https://mx.linkedin.com/company/amk-technologies)  
* **Tecnologías**  
    - Java  
    - Django  
    - Docker  
    - Hadoop  
    - GitLab  
    - Python  
    - iOS  
    - Android  
    - Jenkins  
    - Nginx  
